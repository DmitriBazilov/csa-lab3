from __future__ import annotations

import re
from enum import Enum
from typing import Final


class Commands(str, Enum):
    nop = "nop"
    add = "add"
    sub = "sub"
    mul = "mul"
    div = "div"
    mod = "mod"
    xor = "xor"
    and_ = "and"
    or_ = "or"
    cmp = "cmp"
    mov = "mov"
    mova = "mova"
    movi = "movi"
    movo = "movo"
    jmp = "jmp"
    jz = "jz"
    jnz = "jnz"
    jn = "jn"
    jp = "jp"
    iret = "iret"
    di = "di"
    ei = "ei"
    hlt = "hlt"

    def __str__(self) -> str:
        return self.value


class Registers(str, Enum):
    rax = "rax"
    rbx = "rbx"
    rdx = "rdx"
    rcx = "rcx"
    rip = "rip"
    rst = "rst"
    rsp = "rsp"

    def __str__(self) -> str:
        return self.value


def parse_instruction(byte_string: bytes):
    command = byte_string[0]
    arg_types = byte_string[1]
    arg1_type = arg_types >> 4
    arg2_type = arg_types & 0xF
    is_long: bool = arg1_type == 1 and arg2_type in [0, 2, 4]
    return command, arg1_type, arg2_type, is_long


registers: list[Registers] = [
    Registers.rax,
    Registers.rbx,
    Registers.rdx,
    Registers.rcx,
    Registers.rip,
    Registers.rst,
    Registers.rsp,
]
branch_commands: list[Commands] = [Commands.jmp, Commands.jz, Commands.jnz, Commands.jn, Commands.jp]
alu_commands: list[Commands] = [
    Commands.add,
    Commands.sub,
    Commands.mul,
    Commands.div,
    Commands.mod,
    Commands.xor,
    Commands.and_,
    Commands.or_,
    Commands.cmp,
]
two_op_commands: list[Commands] = [*alu_commands, Commands.mov, Commands.movi, Commands.movo, Commands.mova]
one_op_commands: list[Commands] = branch_commands
zero_op_commands: list[Commands] = [Commands.nop, Commands.iret, Commands.di, Commands.ei, Commands.hlt]
op_commands: list[Commands] = [*two_op_commands, *one_op_commands, *zero_op_commands]

SECTION_DATA: Final = "section .data"
SECTION_TEXT: Final = "section .text"
RE_STR: Final = r"^\".*\"$"
RE_INT: Final = r"^[+-]?\d+$"


def is_integer(value: str) -> bool:
    return bool(re.match(RE_INT, value))


def is_string(value: str) -> bool:
    return bool(re.match(RE_STR, value))
