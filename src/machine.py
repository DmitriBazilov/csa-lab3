from __future__ import annotations

import logging
import struct
import sys
from typing import Callable

from src.isa import (
    Commands,
    Registers,
    is_integer,
    op_commands,
    parse_instruction,
    registers,
    two_op_commands,
    zero_op_commands,
)


class ALU:
    def __init__(self):
        self.N: bool = False
        self.Z: bool = True

    def __str__(self):
        return f"N = {self.N}, Z = {self.Z}"

    def set_flags(self, val):
        self.N = val < 0
        self.Z = val == 0

    def get_flags(self) -> tuple[bool, bool]:
        return self.N, self.Z

    def do_op(self, left: int, right: int, func: Callable[[int, int], int]) -> int:
        res = func(left, right)
        self.set_flags(res)
        return res


class Datapath:
    alu: ALU = ALU()

    def __init__(self, memory: list[bytes], ports):
        self.data_memory_size = 65536
        self.data_memory = memory
        self.ports = ports
        self.data_memory.extend([b"\x00\x00\x00\x00" for _ in range(self.data_memory_size - len(memory))])
        self.registers = {name: 0 for name in registers}
        self.set_reg(Registers.rsp, self.data_memory_size)

    def get_reg(self, register: Registers) -> int:
        return self.registers[register]

    def set_reg(self, register: Registers, value: int):
        self.registers[register] = value

    def read(self, register: Registers, address: int):
        self.set_reg(register, struct.unpack(">i", self.data_memory[address])[0])

    def write(self, address: int, value: int):
        self.data_memory[address] = struct.pack(">i", value)

    def read_port(self, register: Registers, port: int):
        if is_integer(str(self.ports[port][0][1])):
            logging.info("input: " + str(self.ports[port][0][1]))
            self.set_reg(register, self.ports[port].pop(0)[1])
        else:
            logging.info("input: " + (self.ports[port][0][1] if self.ports[port][0][1] != "\n" else "\\n"))
            self.set_reg(register, ord(self.ports[port].pop(0)[1]))

    def write_port(self, register: Registers, port: int):
        self.ports[port].append(chr(self.get_reg(register)))
        logging.info(
            "OUTPUT: "
            + str(self.ports[port][:-1])
            + " <- "
            + (str(self.ports[port][-1]) if str(self.ports[port][-1]) != "\n" else "\\n")
        )

    def push(self, value):
        self.set_reg(Registers.rsp, self.get_reg(Registers.rsp) - 1)
        self.write(self.get_reg(Registers.rsp), value)

    def pop(self) -> int:
        self.read(Registers.rst, self.get_reg(Registers.rsp))
        self.set_reg(Registers.rsp, self.get_reg(Registers.rsp) + 1)
        return self.registers[Registers.rst]


class ControlUnit:
    def __init__(self, datapath: Datapath, program_memory: list[bytes], limit: int):
        self.datapath = datapath
        self.program_memory: list[bytes] = program_memory
        self.limit = limit
        self.instr_counter = 0
        self.tick = 0
        self.long_instr = 0
        self.instr: bytes = b""
        self.left_arg: tuple[int, int] = (-1, 0)
        self.right_arg: tuple[int, int] = (-1, 0)
        self.mode = ""
        self.interupt_state = 0  # 0 - di, 1 - ei
        self.command: Commands = Commands.nop

    def inc_tick(self):
        self.tick += 1

    def get_tick(self):
        return self.tick

    def command_cycle(self):
        logging.info("%s", self)
        try:
            while self.instr_counter < self.limit:
                self.instruction_cycle()
                self.operand_cycle()
                self.execution_cycle()
                logging.info("%s", self)
                self.interuption_cycle()
        except StopIteration:
            logging.info("%s", self)
        except EOFError:
            logging.warning("code is end")

    def check_input(self):
        input_port = self.datapath.ports[0]
        if len(input_port) and input_port[0][0] <= self.get_tick():
            return True
        return False

    def execute_interuption(self):
        self.inc_tick()
        self.mode = "TRAP:"
        self.interupt_state = 0
        self.inc_tick()
        self.datapath.push(self.datapath.get_reg(Registers.rip))
        self.inc_tick()
        self.datapath.push(self.datapath.get_reg(Registers.rax))
        self.inc_tick()
        self.datapath.read(Registers.rip, 0)

    def interuption_cycle(self):
        if self.interupt_state == 0:
            return
        if self.check_input():
            self.execute_interuption()

    def instruction_cycle(self):
        if self.datapath.get_reg(Registers.rip) >= len(self.program_memory):
            raise EOFError
        self.instr_counter += 1
        self.inc_tick()
        self.instr = self.program_memory[self.datapath.registers[Registers.rip]]
        cmd, f_arg_type, _, is_long = parse_instruction(self.instr)
        self.command = op_commands[cmd]
        self.long_instr = is_long
        self.datapath.set_reg(Registers.rip, self.datapath.registers[Registers.rip] + 1)

    def get_first_arg(self, cmd, first_type, second_type) -> tuple[int, int]:
        if op_commands[cmd] in zero_op_commands:
            return -1, 0
        if first_type == 0:
            return first_type, struct.unpack(">H", self.instr[2:4])[0]
        if first_type == 1:
            return first_type, self.datapath.get_reg(registers[self.instr[2]])
        return -1, 0

    def get_second_arg(self, cmd, first_type, second_type) -> tuple[int, int]:
        if op_commands[cmd] in two_op_commands and first_type == 0 and second_type == 1:
            res = struct.unpack(">i", self.program_memory[self.datapath.get_reg(Registers.rip)])[0]
            self.datapath.set_reg(Registers.rip, self.datapath.get_reg(Registers.rip) + 1)
            return second_type, self.datapath.get_reg(registers[res])
        if op_commands[cmd] in two_op_commands and second_type in [0, 2, 4]:
            self.inc_tick()
            res = struct.unpack(">i", self.program_memory[self.datapath.get_reg(Registers.rip)])[0]
            self.datapath.set_reg(Registers.rip, self.datapath.get_reg(Registers.rip) + 1)
            if second_type in [2, 4]:
                self.inc_tick()
                res = struct.unpack(">i", self.datapath.data_memory[res])[0]
            if second_type == 4:
                self.inc_tick()
                res = struct.unpack(">i", self.datapath.data_memory[res])[0]
            return second_type, res
        if op_commands[cmd] in two_op_commands and second_type in [1, 3]:
            self.inc_tick()
            return second_type, self.datapath.get_reg(registers[self.instr[3]]) if second_type == 1 else self.instr[3]
        return -1, 0

    def operand_cycle(self):
        cmd, f_arg_type, s_arg_type, _ = parse_instruction(self.instr)
        self.left_arg = self.get_first_arg(cmd, f_arg_type, s_arg_type)
        self.right_arg = self.get_second_arg(cmd, f_arg_type, s_arg_type)
        return

    def execution_cycle(self):
        cmd, f_arg_type, s_arg_type, is_long = parse_instruction(self.instr)
        byte3 = struct.unpack("B", self.instr[2:3])[0]
        f_reg = registers[byte3] if f_arg_type == 1 else 0
        command = op_commands[cmd]
        if command == Commands.nop:
            return
        self.inc_tick()
        match command:
            case Commands.add:
                res = self.datapath.alu.do_op(self.left_arg[1], self.right_arg[1], lambda a, b: a + b)
                self.datapath.set_reg(f_reg, res)
            case Commands.sub:
                res = self.datapath.alu.do_op(self.left_arg[1], self.right_arg[1], lambda a, b: a - b)
                self.datapath.set_reg(f_reg, res)
            case Commands.mul:
                res = self.datapath.alu.do_op(self.left_arg[1], self.right_arg[1], lambda a, b: a * b)
                self.datapath.set_reg(f_reg, res)
            case Commands.div:
                res = self.datapath.alu.do_op(self.left_arg[1], self.right_arg[1], lambda a, b: a // b)
                self.datapath.set_reg(f_reg, res)
            case Commands.mod:
                res = self.datapath.alu.do_op(self.left_arg[1], self.right_arg[1], lambda a, b: a % b)
                self.datapath.set_reg(f_reg, res)
            case Commands.xor:
                res = self.datapath.alu.do_op(self.left_arg[1], self.right_arg[1], lambda a, b: a ^ b)
                self.datapath.set_reg(f_reg, res)
            case Commands.and_:
                res = self.datapath.alu.do_op(self.left_arg[1], self.right_arg[1], lambda a, b: a & b)
                self.datapath.set_reg(f_reg, res)
            case Commands.or_:
                res = self.datapath.alu.do_op(self.left_arg[1], self.right_arg[1], lambda a, b: a | b)
                self.datapath.set_reg(f_reg, res)
            case Commands.cmp:
                self.datapath.alu.do_op(self.left_arg[1], self.right_arg[1], lambda a, b: a - b)
            case Commands.mov:
                self.mov(f_reg)
            case Commands.movi:
                self.datapath.read_port(f_reg, self.right_arg[1])
            case Commands.movo:
                self.datapath.write_port(f_reg, self.right_arg[1])
            case Commands.mova:
                self.datapath.write(self.right_arg[1], self.left_arg[1])
            case Commands.iret:
                self.mode = ""
                self.datapath.set_reg(Registers.rax, self.datapath.pop())
                self.inc_tick()
                self.datapath.set_reg(Registers.rip, self.datapath.pop())
                self.interupt_state = 1
            case Commands.di:
                self.interupt_state = 0
            case Commands.ei:
                self.interupt_state = 1
            case Commands.jmp:
                self.datapath.set_reg(Registers.rip, self.left_arg[1])
            case Commands.jz:
                if self.datapath.alu.get_flags()[1]:
                    self.datapath.set_reg(Registers.rip, self.left_arg[1])
            case Commands.jnz:
                if not self.datapath.alu.get_flags()[1]:
                    self.datapath.set_reg(Registers.rip, self.left_arg[1])
            case Commands.jn:
                if self.datapath.alu.get_flags()[0]:
                    self.datapath.set_reg(Registers.rip, self.left_arg[1])
            case Commands.jp:
                if not self.datapath.alu.get_flags()[0]:
                    self.datapath.set_reg(Registers.rip, self.left_arg[1])
            case Commands.hlt:
                raise StopIteration

    def mov(self, register):
        if self.left_arg[0] == 0 and self.right_arg[0] == 1:
            self.datapath.write(self.left_arg[1], self.right_arg[1])
            return
        self.datapath.set_reg(register, self.right_arg[1])
        self.datapath.alu.set_flags(self.right_arg[1])

    def __repr__(self):
        regs = ", ".join(f"{name}: {value}" for name, value in self.datapath.registers.items())
        return (
            f"{self.mode}Tick: {self.get_tick()} | Registers: {regs} "
            f"Flags: {self.datapath.alu} | Command: {self.command} | Instruction: {self.instr.hex()}"
        )


def simulation(memory, instructions, input_tokens):
    datapath = Datapath(memory, {0: input_tokens, 1: []})
    control_unit = ControlUnit(datapath, instructions, 1000)
    control_unit.command_cycle()
    logging.info("output buffer: %s", repr("".join(datapath.ports[1])))
    return "".join(datapath.ports[1]), control_unit.instr_counter, control_unit.get_tick()


def main(text_path, memory_path, input_path):
    memory = []
    instructions = []
    with open(memory_path, "rb") as fm:
        while len(m := fm.read(4)) != 0:
            memory.append(m)
    with open(text_path, "rb") as ft:
        while len(t := ft.read(4)) != 0:
            instructions.append(t)
    with open(input_path) as fi:
        inp = fi.read()
    output, instr_counter, ticks = simulation(memory, instructions, eval(inp))
    print(output)
    print("instr_counter:", instr_counter, "ticks:", ticks, sep=" ")


if __name__ == "__main__":
    assert len(sys.argv) == 4, "Wrong arguments: machine.py <memory_file> <code_file> <input_file>"
    logging.getLogger().setLevel(logging.DEBUG)
    _, memory_file, text_file, input_file = sys.argv
    main(text_file, memory_file, input_file)
