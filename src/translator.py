from __future__ import annotations

import re
import struct
import sys

from src.isa import (
    SECTION_DATA,
    SECTION_TEXT,
    Commands,
    Registers,
    is_integer,
    is_string,
    one_op_commands,
    op_commands,
    registers,
    zero_op_commands,
)


def clean_comments(line_code: str) -> str:
    return re.sub(r";.*", "", line_code)


def clean_code(asm_code: str) -> str:
    lines: list[str] = asm_code.splitlines()
    cleaned_lines = map(clean_comments, lines)
    cleaned_lines = filter(bool, cleaned_lines)
    cleaned_lines = map(str.strip, cleaned_lines)
    cleaned_code: str = "\n".join(cleaned_lines)
    return cleaned_code


def parse_arg(arg: str, variables: dict[str, int], labels: dict[str, int]) -> str:
    if is_integer(arg) or arg.startswith("%") or arg.startswith("!"):
        return f" {arg}"
    if arg.startswith(("#", "*")):
        if is_integer(arg[1:]):
            return f" {arg[0]}{arg[1:]!s}"
        return f" {arg[0]}{variables[arg[1:]]!s}"
    if arg.startswith("&"):
        return f" {variables[arg[1:]]}"
    if arg.startswith("."):
        return f" {labels[arg]!s}"
    name, offset = arg.split("[")
    return f" #{variables[name] + int(offset[:-1])!s}"


def is_long(arg: list[str]) -> bool:
    return len(arg) > 2 and (arg[1][0], arg[2][0]) != ("%", "%") and (arg[1][0], arg[2][0]) != ("%", "!")


def parse_text_section(code: str, variables: dict[str, int]) -> list[str]:
    lines: list[str] = []
    labels: dict[str, int] = {}
    offset = 0
    for idx, code_line in enumerate(code.splitlines()):
        if code_line.startswith("."):
            if code_line.find(" ") != -1:
                raise SyntaxError()
            labels[code_line] = idx - (len(labels)) + offset
        else:
            spl = code_line.split()
            offset += 1 if is_long(spl) else 0
            lines.append(code_line)
    for idx, line in enumerate(lines):
        cmd_split = line.split()
        if not cmd_split[0].startswith("."):
            command: str = cmd_split[0]
            for arg in cmd_split[1:]:
                command += parse_arg(arg, variables, labels)
                lines[idx] = command
    return lines


def parse_data_section(code: str) -> tuple[dict[str, int], list[int]]:
    variables: dict[str, int] = {"ADDR_TO_INT": 0}
    memory: list[int] = [0]
    for code_line in code.splitlines():
        var, val = code_line.split(":")
        var = var.strip()
        val = val.strip()
        if is_integer(val):
            variables[var] = len(memory)
            memory.append(int(val))
        elif is_string(val):
            variables[var] = len(memory)
            string_data = val[1:-1]
            memory.append(len(string_data))
            memory += [ord(char) for char in string_data]
        elif val.startswith("buf "):
            variables[var] = len(memory)
            buf_params = val.split()
            if not is_integer(buf_params[1]):
                raise ValueError()
            memory += [0 for _ in range(int(buf_params[1]))]
        elif val in list(variables.keys()):
            variables[var] = len(memory)
            memory.append(variables[val])
        else:
            raise ValueError()
    return variables, memory


def get_arg(arg: str) -> tuple[int, int]:
    if arg.startswith("%"):
        return 1, registers.index(Registers(arg[1:]))
    if arg.startswith("#"):
        return 2, int(arg[1:])
    if arg.startswith("!"):
        return 3, int(arg[1:])
    if arg.startswith("*"):
        return 4, int(arg[1:])
    return 0, int(arg)


def get_bin_args(command: str, args: list[str]) -> tuple[bytes, str]:
    if Commands(command) in zero_op_commands:
        return struct.pack(">Bxx", 0), format(0, "06x")
    if Commands(command) in one_op_commands:
        f_arg = args[0]
        t, b = get_arg(f_arg)
        t *= 16
        return struct.pack(">BH", t, b), f"{t:02x}{b:04x}"
    f_arg = args[0]
    ft, fb = get_arg(f_arg)
    s_arg = args[1]
    st, sb = get_arg(s_arg)
    types = ft * 16 + st
    if ft == 0:
        return struct.pack(">BHi", types, fb, sb), f"{types:02x}{fb:04x}{sb:08x}"
    if ft == 1 and st in [1, 3]:
        return struct.pack(">BBB", types, fb, sb), f"{types:02x}{fb:02x}{sb:02x}"
    return struct.pack(">BBBi", types, fb, 0, sb), f"{types:02x}{fb:02x}00{sb:08x}"


def convert_to_binary(text: list[str], memory: list[int], asm: list[str]) -> tuple[list[bytes], list[bytes], list[str]]:
    debug: list[str] = []
    bin_memory: list[bytes] = []
    for data in memory:
        bin_memory.append(struct.pack(">i", data))
    bin_text: list[bytes] = []
    offset = 0
    for index, line in enumerate(text):
        debug_line = ""
        words = line.split()
        command = words[0]
        idx = op_commands.index(Commands(command))
        bin_text.append(struct.pack("B", idx))
        debug_line += format(index + offset, "05") + " - " + format(idx, "02x")
        args_bytes = get_bin_args(command, words[1:])
        bin_text.append(args_bytes[0])
        debug_line += args_bytes[1]
        offset += 1 if len(args_bytes[0]) > 3 else 0
        debug.append(debug_line + " - " + asm[index] + "\n")

    return bin_memory, bin_text, debug


def translate(code: str) -> tuple[list[bytes], list[bytes], list[str]]:
    text: int = code.find(SECTION_TEXT)
    data_code = code[len(SECTION_DATA) + 1 : text - 1]
    text_code = code[text + len(SECTION_TEXT) + 1 :]
    variables, memory = parse_data_section(data_code)
    text_lines = parse_text_section(text_code, variables)
    asm_lines_filtered = list(filter(lambda line: not line.strip().startswith("."), text_code.splitlines()))
    return convert_to_binary(text_lines, memory, asm_lines_filtered)


def write_bin_to_file(target_memory, target_text, bin_memory, bin_text):
    with open(target_memory, "wb") as fm:
        for byte in bin_memory:
            fm.write(byte)
    with open(target_text, "wb") as ft:
        for byte in bin_text:
            ft.write(byte)


def write_debug_to_file(debug, filename, memory):
    with open(filename, "w") as fd:
        for line in debug:
            fd.write(line)
        fd.write("==============================\n")
        for m in memory:
            fd.write(str(struct.unpack(">i", m)[0]) + "\n")


def main(source, target_memory, target_text, target_log):
    with open(source, encoding="utf-8") as f:
        source = f.read()

    cleaned_code = clean_code(source)
    bin_memory, bin_text, debug = translate(cleaned_code)
    write_bin_to_file(target_memory, target_text, bin_memory, bin_text)
    write_debug_to_file(debug, target_log, bin_memory)
    print("source LoC:", len(source.split("\n")), "code instr:", len(debug))


if __name__ == "__main__":
    assert len(sys.argv) == 5, "Illegal argument"
    _, src, target_mem, target_code, target_log = sys.argv
    main(src, target_mem, target_code, target_log)
