import contextlib
import io
import logging
import os
import tempfile

import pytest
from src import machine, translator


@pytest.mark.golden_test("../golden/*.yml")
def test_translator_and_machine(golden, caplog):
    caplog.set_level(logging.INFO)

    with tempfile.TemporaryDirectory() as tmp_dir_name:
        source = os.path.join(tmp_dir_name, "source")
        input_stream = os.path.join(tmp_dir_name, "input")
        target_text = os.path.join(tmp_dir_name, "target_text")
        target_memory = os.path.join(tmp_dir_name, "target_memory")
        target_log = os.path.join(tmp_dir_name, "target_log")

        with open(source, "w", encoding="utf-8") as file:
            file.write(golden["in_source"])
        with open(input_stream, "w", encoding="utf-8") as file:
            file.write(golden["in_stdin"])

        with contextlib.redirect_stdout(io.StringIO()) as stdout:
            translator.main(source, target_memory, target_text, target_log)
            print("============================================================")
            machine.main(target_text, target_memory, input_stream)

        with open(target_log, encoding="utf-8") as file:
            log = file.read()

        assert log == golden.out["translator_log"]
        assert stdout.getvalue() == golden.out["out_stdout"]
        assert caplog.text == golden.out["machine_log"]
