# csa-lab3

- P33141, Базилов Дмитрий Валерьевич
- asm | cisc | harv | hw | instr | binary | trap | port | pstr | prob2 | spi
- Усложнение не реализовано

## Язык программирования

### Синитаксис

#### Форма Бэкуса-Нуара

```ebnf
<program>       ::= <section_text> | <section_data> <section_text>

<section_data>  ::= "section .data \n" <data_context>
<data_context>  ::= <data> | <data> <data_context>
<data>          ::= <var_name> ":" <var_value> "\n" | <var_name> ":" <var_value> <comment> "\n"
<var_name>      ::= [a-z] <word> | [A-Z] <word>
<var_value>     ::= <string> | <number> | <buffer>
<string>        ::= "'" <text> "'"
<buffer>        ::= "buf " <number>

<section_text>  ::= "section .text" <instr_ctx>
<instr_ctx>     ::= <instruction> | <isntruction> <instr_ctx>
<instruction>   ::= <command> "\n" | <command> <comment> "\n"
<command>       ::= <lang_word> | <lang_word> <operands>
<lang_word>     ::= <var_name>
<operands>      ::= <operand> | <operand> <operand>
<operand>       ::= <number> | <label> | <register> | <var> | <port>
<label>         ::= "." <lang_word>
<register>      ::= "%" <lang_word>
<ind_addr>      ::= "*" <lang_word>
<var_addr>      ::= "&" <lang_word>
<var>           ::= "#" <lang_word> | <lang_word> <array_index>
<array_index>   ::= "[" <number> "]" | "[" <lang_word> "]"
<port>          ::= "!" <number>

<number>        ::= <digit> | <digit> <number>
<digit>         ::= [0-9]
<comment>       ::= ";" <text>
<text>          ::= <word> | <word> <whitespaces> | <word> <whitespaces> <text>
<word>          ::= <alpha_numeric> | <alpha_numeric> <word>
<alpha_numeric> ::= <letter> | <digit>
<letter>        ::= [a-z] | [A-Z]
<whitespaces>   ::= <whitespace> | <whitespace> <whitespaces>
<whitespace>    ::= "\s"
```

Программа делится на раздел с переменными (section .data) и раздел с кодом (section .text)

Переменные могут быть трех типов: числа, строки и массивы.

В разделе с кодом команды выполняются последовательно. 
У разных команд разное количество операндов.
Сами операнды могут быть разных типов:
- Прямая передача числа
- Прямая адресация - обозначается символом "#"
- Косвенная адресация - обозначается символом "*"
- Адрес переменной - символ "&" (mov %rax &var)
- Регистр - обозначается символом "%"
- Номер порта - обозначается символом "!"

Также, поддерживается механизм меток ``.label_name``, 
которые могут являться операндами для некоторых команд.

## Организация памяти

- Гарвардская архитектура
- Машинное слово - 32 бита
- Память может содержать до 2^16 ячеек

**Графическое представление памяти**
```
    Instruction memory
    +-----------------+
    | 0: INT_H_ADDR   |
    +-----------------+
    | 1: program start|
    |    ...          |
    | l: int handler  |
    | l+1: int handler|
    |   ...           |
    +-----------------+
    
    Data memory
    +-----------------+
    |                 |
    |       DATA      |
    |                 |
    +-----------------|
    |                 |
    |      STACK      |
    |                 |
    +-----------------+
```

- INT_H_ADDR - Адресс первой инструкции обработчика прерывания
- Далее идет код разработчика, описанный в section .text
- DATA - данные хранятся в том же порядке, в котором их объявил программист
- STACK - часть data memory, в которую сохраняются данные при прерывании.

## Система команд

CISC-архитектура по варианту. Доступные регистры: 

- rax - регистр общего назначения, сохраняется на стэк при прерывании
- rbx - регистр общего назначения
- rdx - регистр общего назначения
- rcx - регистр общего назначения
- rip - регистр текущей инструкции, сохраняется на стэк при прерывании
- rsp - указатель стека

Особенности процессора:

Инструкция - 32 - 64 бита
```
|1--------|2--------|3--------|4--------|5--------|6--------|7--------|8--------|
| opcode  |arg1+arg2|  reg_1  |  reg_2  |
                    |   reg   |  port   |
                    |   reg   |         |         immed-value or address        |
                    |      address      |                  reg                  |
                    |      address      |                                            
```

Типы аргументов:
- 0000 - прямая загрузка
- 0001 - регистр
- 0010 - прямая адресация в память
- 0011 - порт
- 0100 - косвенная адресация в память
 
| Syntax | Code | Mnemonic     | Comment                                   |
|:-------|------|:-------------|:------------------------------------------|
| `ADD`  | 00   | summary      | Сложение операндов и сохранение           |
| `SUB`  | 01   | subtract     | Вычитание операндов и сохранение          |
| `MUL`  | 02   | multiply     | Умножение операндов и сохранение          |
| `DIV`  | 03   | divide       | Деление операндов и сохранение            |
| `MOD`  | 04   | module       | Остаток от деления                        |
| `XOR`  | 05   | xor          | Логическое исключающее "ИЛИ" и сохранение |
| `AND`  | 06   | and          | Логическое "И" и сохранение               |
| `OR`   | 07   | or           | Логическое "ИЛИ" и сохранение             |
| `CMP`  | 08   | compare      | Вычитание операндов и установка флагов    |
| `MOV`  | 09   | move         | Передать значение из операнда в операнд   |
| `MOVI` | 0A   | move in      | Передать значение из порта в операнд      |
| `MOVO` | 0B   | move out     | Передать значение из операнда в порт      |
| `MOVA` | 0C   | move in addr | Сохранить значение из регистра в адресс   |
| `JMP`  | 0D   | jmp          | Безусловный переход к метке               |
| `JZ`   | 0E   | jmp zero     | Переход, если равенство (Z=1)             |
| `JNZ`  | 0F   | jmp not zero | Переход, если не равенство (Z=0)          |
| `JN`   | 10   | jmp negative | Переход, если меньше (N=1)                |
| `JP`   | 11   | jmp positive | Переход, если больше или равно (N=0)      |
| `NOP`  | 12   | no operation | Отсутствие операции                       |
| `IRET` | 13   | iret         | Возврат из прерывания                     |
| `DI`   | 14   | di           | Запрет прерываний                         |
| `EI`   | 15   | ei           | Разрешение прерываний                     |
| `HLT`  | 16   | halt         | Остановка программы                       |

## Транслятор

Интерфейс командной строки: ``translator.py <source> <target_memory> <target_text> <target_log>``

Реализовано в [translator](./src/translator.py)

Этапы трансляции:

1. Очистка кода от лишних строк и комментариев
2. Проход по секции памяти
3. Два прохода по секции с кодом: первый заменяет метки на хардкод адреса, второй парсит строки кода, выделяя адресацию.
4. Генерация машинного кода

На вход подается:

1. Путь до файла с исходным кодом
2. Путь до файла, в который будут сохранены данные
3. Путь до файла, в который будут сохранены инструкции
4. Путь до файла с логами(нужно для тестов)

## Пример использования языка

Программа:
```nasm
section .data
    char: 0
    stop_char: 10
section .text
    mov %rax .read_char
    st %rax &ADDR_TO_INT
    ei
    .loop
        mov %rax #char
        cmp %rax #stop_char
        jz .end
        jmp .loop
    .read_char
        movi %rdx !0
        st %rdx &char
        movo %rdx !1
        iret
    .end
        hlt
```

Трансляция происходит в нечитаемый бинарный файл, в этой ситуации помогает отладочный файл(до черты - инструкции, после - данные):

``` 
00000 - 091000000000000b - mov %rax .read_char
00002 - 0c10000000000000 - mova %rax &ADDR_TO_INT
00004 - 15000000 - ei
00005 - 0912000000000001 - mov %rax #char
00007 - 0812000000000002 - cmp %rax #stop_char
00009 - 0e000010 - jz .end
00010 - 0d000005 - jmp .loop
00011 - 0a130200 - movi %rdx !0
00012 - 0c10020000000001 - mova %rdx &char
00014 - 0b130201 - movo %rdx !1
00015 - 13000000 - iret
00016 - 16000000 - hlt
==============================
0
0
10
```

Лог работы процессора можно посмотреть в golden тесте

## Модель процессора

Интерфейс командной строки: ``machine.py <memory_file> <code_file> <input_file>``

Реализовано в [machine.py](./src/machine.py)

* Аргументами принимает: файл с данными, кодом и пользовательским вводом
* Выполняется до инструкции hlt, или до конца программы
* Инструкции имеют три стадии выполнения(кроме nop и branch инструкций)
  * instruction_cycle
  * operand_cycle(может быть пропущен)
  * execution_cycle(может быть пропущен)

### Datapath

![Datapath](resource/Datapath.png)

* `sel_reg` - выбор регистра для операции
* `sel_l` `sel_r` - выбор операнда на АЛУ
* `sel_dr_out` `sel_dr_in` - для чтения из DR и запись в него
* Регистр может быть аргументом как слева, так и справа(Для этого ему нужно пройти через алу и попасть в arg)

### ControlUnit

![ControlUnit](resource/ControlUnit.png)

* `sel_arg` - для выбора аргумента из декодера или из datapath
* `arg_out` - определяет чем является наш аргумент(адресом для ветвления, адресом или конечным аргументом)
* `sel_next` - инкрементирует rip, если команда ветвления то приравнивает rip к аргументу

## Тестирование
Реализованные програмы

1. [hello world](golden/hello.yml): вывести на экран строку `'Hello World!'`
2. [cat](golden/cat.yml): программа `cat`, повторяем ввод на выводе.
3. [hello_user_name](golden/hello_user_name.yml) -- программа `hello_user_name`: запросить у пользователя его
   имя, считать его, вывести на экран приветствие
4. [prob2](golden/prob2.yml): Найти сумму четных членов последовательности Фиббонначи до 4000000

Интеграционные тесты реализованы тут [integration_test](./tests/integration_test.py):

- через golden tests, конфигурация которых лежит в папке [golden](./golden)

CI: Нагло взято [отсюда](https://gitlab.se.ifmo.ru/computer-systems/csa-rolling/-/blob/2023/.gitlab-ci.yml)

```yaml
lab3-example:
  stage: test
  image:
    name: ryukzak/python-tools
    entrypoint: [""]
  script:
    - poetry install
    - coverage run -m pytest --verbose
    - find . -type f -name "*.py" | xargs -t coverage report
    - ruff format --check .
    - ruff check .
```

## Итог
```text
| ФИО                        | алг              | LoC | code байт | code инстр. | инстр. | такт. | вариант                                                                    |
| Базилов Дмитрий Валерьевич | hello            | 18  | 76        | 12          | 99     | 298   | asm | cisc | harv | hw | instr | binary | trap | port | pstr | prob2 | spi |
| Базилов Дмитрий Валерьевич | cat              | 19  | 68        | 12          | 35     | 128   | asm | cisc | harv | hw | instr | binary | trap | port | pstr | prob2 | spi |
| Базилов Дмитрий Валерьевич | hello_user_name  | 79  | 340       | 51          | 283    | 898   | asm | cisc | harv | hw | instr | binary | trap | port | pstr | prob2 | spi |
| Базилов Дмитрий Валерьевич | prob1            | 61  | 252       | 44          | 498    | 1409  | asm | cisc | harv | hw | instr | binary | trap | port | pstr | prob2 | spi |
```

